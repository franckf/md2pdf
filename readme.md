**stand-by**

`md2pdf` is a simple tool to convert markdown file to pdf.

## choice of pdf lib

options are:

- [gopdf](https://github.com/signintech/gopdf) : nice and simple
- [maroto](https://github.com/johnfercher/maroto) : nice and simple but different formating in code
- [godpdf](https://github.com/jung-kurt/gofpdf) : reference but unmaintened
- [unipdf](https://github.com/unidoc/unipdf) : commercial licence

## features to come

- [ ] formating

```
_italic text_
_italic text_
**my bold text**
**my bold text**
**_my bold and italic text_**
**_my bold and italic text_**
```

- [ ] jump line, two spaces at the end

- [x] headings

```
# Heading level 1
## Heading level 2
### Heading level 3
#### Heading level 4
##### Heading level 5
###### Heading level 6
```

- [ ] links

```
View the page by [clicking here](https://url.com)
```

- [ ] images

```
![message](screenshots/image.png)
```

- [ ] quotes

```
> Blockquote
>
> > subBlockquote
```

- [ ] lists

```
1. Ordered List
2. Ordered List

3) Ordered List
4) Ordered List

- Unordered List
- Unordered List

* Unordered List
* Unordered List
```

- [ ] code

```
      code

  sentence `code` follow

\`\`\`bash
code
\`\`\`
```

- [ ] lines

```
***
---
```

- [ ] tables

```
|                  | Colonne  | Colonne  |
 ----------------- | -------- | --------
| Ligne            | `Ligne`  | Ligne    |
| Ligne            | Ligne    | Ligne    |
| Ligne            | Ligne    | Ligne    |

Colonne  | Colonne
-------- | ---
Ligne    | Ligne
Ligne    | Ligne
Ligne    | Ligne

| Colonne  | Colonne | Colonne   |
| :------- | ----:   | :---:     |
| Ligne    | Ligne   |  Ligne    |
| Ligne    | Ligne   |  Ligne    |
| Ligne    | Ligne   |  Ligne    |
```

- [ ] definitions

```
Term 1
Term 2
:   Definition A
:   Definition B
```

- [ ] checkbox

```
- [x] This is a complete item
- [ ] This is an incomplete item
```

- [ ] generate table of content
