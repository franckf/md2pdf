package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"time"

	"github.com/signintech/gopdf"
)

var defaultFont string = "DejaVuSansMono"
var headingSize = [7]int{9, 20, 18, 16, 14, 12, 10}
var defaultFontSize int = headingSize[0]
var defaultSeparatorSize float64 = float64(headingSize[6])

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage: md2pdf input.md ouput.pdf")
		os.Exit(1)
	}

	inputMD := os.Args[1]
	outputPDF := os.Args[2]

	lines := openMD(inputMD)
	createPDF(lines, outputPDF)
}

func openMD(inputMD string) []string {
	file, err := os.Open(inputMD)
	if err != nil {
		fmt.Println("Error opening markdown file:", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	lineByLine := []string{}

	for scanner.Scan() {
		line := scanner.Text()
		lineByLine = append(lineByLine, line)
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Error while reading scan of file:", err)
	}

	return lineByLine
}

func createPDF(lines []string, outputPDF string) {
	pdf := gopdf.GoPdf{}
	pdf.Start(gopdf.Config{
		PageSize: *gopdf.PageSizeA4, //595.28, 841.89 = A4
	})
	pdf.SetMarginTop(30)
	pdf.SetMarginBottom(30)
	pdf.SetMarginLeft(20)
	pdf.SetMarginRight(20)
	pdf.AddPage()

	err := pdf.AddTTFFont(defaultFont, "./DejaVuSansMono.ttf")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	err = pdf.SetFont(defaultFont, "", defaultFontSize)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	for _, line := range lines {
		if pdf.GetY() >= gopdf.PageSizeA4.H-pdf.MarginBottom() {
			pdf.AddPage()
		}
		if line == "" {
			pdf.Br(defaultSeparatorSize)
		}
		pdfishLine(&pdf, line)
	}

	pdf.SetInfo(gopdf.PdfInfo{
		Title:        outputPDF,
		Creator:      "md2pdf",
		CreationDate: time.Time{},
	})
	pdf.WritePdf(outputPDF)
}

func pdfishLine(pdf *gopdf.GoPdf, line string) {
	fullText(pdf, line)
}

func fullText(pdf *gopdf.GoPdf, line string) {
	lineWidth, _ := pdf.MeasureTextWidth(line)
	spaceWidth, _ := pdf.MeasureTextWidth(" ")
	textArea := gopdf.PageSizeA4.W - pdf.MarginRight() - pdf.MarginLeft()

	if lineWidth >= textArea {
		limit := int(textArea / spaceWidth)
		sentenceBreaks := sentenceBreaker(line, limit)
		for _, sentenceBreak := range sentenceBreaks {
			pdf.Text(sentenceBreak)
			pdf.Br(defaultSeparatorSize)
		}
	} else {
		pdf.Text(line)
		pdf.Br(defaultSeparatorSize)
	}
}

func searchHeading(pdf *gopdf.GoPdf, line string) {
	heading, _ := regexp.Compile(`^#{1,6} `)
	if heading.Match([]byte(line)) {
		levelOfHeading := len(heading.Find([]byte(line)))
		pdf.SetFont(defaultFont, "", headingSize[levelOfHeading-1])
		pdf.Br(defaultSeparatorSize)
		fullText(pdf, line[levelOfHeading:])
		pdf.Br(defaultSeparatorSize * 2)
		pdf.SetFont(defaultFont, "", defaultFontSize)
	}
}

func sentenceBreaker(sentence string, limit int) []string {
	sentenceBreaks := []string{}
	cursorPosition := limit

	for len(sentence) > 0 {
		if cursorPosition > len(sentence) {
			sentenceBreaks = append(sentenceBreaks, sentence)
			break
		}
		if sentence[cursorPosition-1] != 32 {
			cursorPosition--
		} else {
			partOfSentence := sentence[:cursorPosition-1]
			sentenceBreaks = append(sentenceBreaks, partOfSentence)
			sentence = sentence[cursorPosition:]
			cursorPosition = limit
		}
	}
	return sentenceBreaks
}
